import Vue from "vue";
import axios from 'axios'
import FlagIcon from 'vue-flag-icon'
import Loading from 'vue-loading-overlay'
import Snotify, { SnotifyPosition } from 'vue-snotify'
import VueAxios from 'vue-axios'
import Vuetify from 'vuetify'

import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.use(VueAxios, axios)
Vue.use(FlagIcon)
Vue.use(Loading, {
	color: '#666',
	isFullPage: true,
	loader: 'dots'
})
Vue.use(Snotify, {
	toast: {
		position: SnotifyPosition.rightTop,
		timeout: 5000
	}
})
Vue.use(Vuetify)

// default axios stuff
Vue.axios.defaults.baseURL = process.env.VUE_APP_API_URL
Vue.axios.defaults.xsrfCookieName = false
delete Vue.axios.defaults.headers.common['X-CSRF-TOKEN']
delete Vue.axios.defaults.headers.common['X-XSRF-TOKEN']

const storedToken = localStorage.getItem(process.env.VUE_APP_TOKEN)
if (storedToken) {
	Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + storedToken
}

// axios interceptors
Vue.axios.interceptors.response.use(undefined, (error) => {
	if (error.response) {
		// handle 422: Unprocessable Entity
		if (error.response.status == 422) {
			store.commit('ERROR_RESET')
			store.commit('APP_ERRORS', error.response.data.errors)
		}

		// handle 401: Unauthorized
		else if (error.response.status == 401) {
			store.commit('ERROR_RESET')
			store.commit('AUTH_LOGOUT')
		}

		else if (error.response.status >= 500) {
			store.commit('ERROR_RESET')
			store.commit('APP_ERROR', 'System Error. Please try again later.')
		}
	}

	store.commit('REQUEST_STOP')

	return Promise.reject(error)
})

// import global CSS files
import 'vuetify/dist/vuetify.min.css'
import 'vue-loading-overlay/dist/vue-loading.css'
import 'vue-snotify/styles/material.scss'

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
