export default {
	methods: {
		getLoggedInUser() {
			let request = this.axios.get('user', {
				withCredentials: false
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		}
	},
	name: 'UsersMixin'
}