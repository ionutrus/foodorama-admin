export default {
	methods: {
		createIngredient(data) {
			let request = this.axios.post('ingredients', data, {
				withCredentials: false
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		},
		getIngredient(id) {
			let request = this.axios.get(`ingredients/${id}`, {
				withCredentials: false
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		},
		getIngredientTranslations(id) {
			let request = this.axios.get(`ingredients/${id}/translations`, {
				withCredentials: false
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		},
		updateIngredient(id, data) {
			let request = this.axios.patch(`ingredients/${id}`, data, {
				withCredentials: false
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		}
	},
	name: 'IngredientsMixin'
}