export default {
	methods: {
		getCategory(id) {
			let request = this.axios.get(`ingredients/categories/${id}`, {
				withCredentials: false
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		},
		getTree(selectable = false) {
			let request = this.axios.get((selectable) ? 'ingredients/categories/tree/selectable' : 'ingredients/categories/tree', {
				withCredentials: false
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		}
	},
	name: 'IngredientsCategoriesMixin'
}