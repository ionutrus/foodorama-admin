import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
	strict: true,
	state: {
		accessToken: localStorage.getItem(process.env.VUE_APP_TOKEN) || '',
		errors: [],
		messages: [],
		status: 'idle',
		user: null
	},
	mutations: {
		APP_ERROR(state, error) {
			state.errors.push(error)
		},
		APP_ERRORS(state, errors) {
			for (var errorKey in errors) {
				errors[errorKey].forEach((error) => {
					state.errors.push(error)
				})
			}
		},
		APP_MESSAGE(state, message) {
			state.messages = []
			state.messages.push(message)
		},
		APP_USER(state, user) {
			state.user = user
		},
		AUTH_LOGOUT(state) {
			state.accessToken = false
			state.user = null

			localStorage.removeItem(process.env.VUE_APP_TOKEN)
			delete Vue.axios.defaults.headers.common['Authorization']
		},
		AUTH_SUCCESS(state, token) {
			state.status = 'success'
			state.accessToken = token
		},
		ERROR_RESET(state) {
			state.errors = []
		},
		REQUEST_INIT(state) {
			state.status = 'loading'
		},
		REQUEST_STOP(state) {
			state.status = 'idle'
		}
	},
	actions: {
		AUTH_REQUEST: ({commit}, credentials) => {
			return new Promise((resolve) => {
				commit('REQUEST_INIT')
				commit('ERROR_RESET')

				Vue.axios.post('login', credentials).then((response) => {
					const token = response.data.access_token,
						user = response.data.user

					localStorage.setItem(process.env.VUE_APP_TOKEN, token)
					Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token

					commit('AUTH_SUCCESS', token)
					commit('APP_USER', user)
					commit('APP_MESSAGE', 'Successfully logged in')
					commit('REQUEST_STOP')

					resolve(response)

				}).catch(() => {
					localStorage.removeItem(process.env.VUE_APP_TOKEN)
					delete Vue.axios.defaults.headers.common['Authorization']
				})
			})
		},
		DEAUTH_REQUEST: ({commit}) => {
			return new Promise((resolve, reject) => {

				commit('REQUEST_INIT')
				Vue.axios.get('logout').then(() => {
					commit('AUTH_LOGOUT')
					commit('APP_MESSAGE', 'Successfully logged out')
					commit('REQUEST_STOP')

					resolve()
				}).catch((error) => {
					commit('AUTH_LOGOUT')
					commit('REQUEST_STOP')

					reject(error)
				})
			})
		}
	},
	getters: {
		isAuthenticated: state => !!state.accessToken
	}
});
