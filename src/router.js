import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
	mode: "history",
	base: process.env.BASE_URL,
	routes: [
		{
		  path: "/",
		  name: "home",
		  component: Home
		},
		{
			path: "/ingredients/categories",
			name: "IngredientCategoriesView",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () =>
				import(/* webpackChunkName: "ingredient_categories_module" */ "./views/ingredients/Categories.vue")
		},
		{
			path: '/ingredients/new',
			name: 'NewIngredientView',
			component: () =>
				import(/* webpackChunkName: "new_ingredient_module" */ "./views/ingredients/NewIngredient.vue")
		},
		{
			path: '/ingredients/:id',
			name: 'ViewIngredientView',
			component: () =>
				import(/* webpackChunkName: "view_ingredient_module" */ "./views/ingredients/ViewIngredient.vue")
		}
	]
});
